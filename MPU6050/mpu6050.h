#ifndef MPU6050_H
#define MPU6050_H

#include <QObject>
#include "../i2c.h"




namespace gyro_private {

#define DMP_MEMORY_BANKS        8
#define DMP_MEMORY_BANK_SIZE    256
#define DMP_MEMORY_CHUNK_SIZE   16

#define pgm_read_byte(x) (*(x))

    class MPU6050 : public QObject, i2c
    {
        Q_OBJECT
    public:
        enum registers {
            // undocumented registers
            XG_OFFS_TC          = 0x00, //[7] PWR_MODE, [6:1] XG_OFFS_TC, [0] OTP_BNK_VLD
            YG_OFFS_TC          = 0x01, //[7] PWR_MODE, [6:1] YG_OFFS_TC, [0] OTP_BNK_VLD
            ZG_OFFS_TC          = 0x02, //[7] PWR_MODE, [6:1] ZG_OFFS_TC, [0] OTP_BNK_VLD
            X_FINE_GAIN         = 0x03, //[7:0] X_FINE_GAIN
            Y_FINE_GAIN         = 0x04, //[7:0] Y_FINE_GAIN
            Z_FINE_GAIN         = 0x05, //[7:0] Z_FINE_GAIN
            XA_OFFS_H           = 0x06, //[15:0] XA_OFFS
            XA_OFFS_L           = 0x07,
            YA_OFFS_H           = 0x08, //[15:0] YA_OFFS
            YA_OFFS_L           = 0x09,
            ZA_OFFS_H           = 0x0A, //[15:0] ZA_OFFS
            ZA_OFFS_L           = 0x0B,
            // documented registers
            SELF_TEST_X         = 0x0D, //[7:5] XA_TEST[4-2], [4:0] XG_TEST[4-0]
            SELF_TEST_Y         = 0x0E, //[7:5] YA_TEST[4-2], [4:0] YG_TEST[4-0]
            SELF_TEST_Z         = 0x0F, //[7:5] ZA_TEST[4-2], [4:0] ZG_TEST[4-0]
            SELF_TEST_A         = 0x10, //[5:4] XA_TEST[1-0], [3:2] YA_TEST[1-0], [1:0] ZA_TEST[1-0]
            XG_OFFS_USRH        = 0x13, //[15:0] XG_OFFS_USR
            XG_OFFS_USRL        = 0x14,
            YG_OFFS_USRH        = 0x15, //[15:0] YG_OFFS_USR
            YG_OFFS_USRL        = 0x16,
            ZG_OFFS_USRH        = 0x17, //[15:0] ZG_OFFS_USR
            ZG_OFFS_USRL        = 0x18,
            SMPLRT_DIV          = 0x19, // Sample Rate Divider
            CONFIG              = 0x1A, // Configuration
            GYRO_CONFIG         = 0x1B, // Gyroscope Configuration
            ACCEL_CONFIG        = 0x1C, // Accelerometer Configuration
            FF_THR              = 0x1D, // Free-fall Threshold
            FF_DUR              = 0x1E, // Free-fall event duration
            MOT_THR             = 0x1F, // Motion Detection Threshold
            MOT_DUR             = 0x20, // Motion Detection Duration
            ZRMOT_THR           = 0x21, // Zero-Motion Detection Threshold
            ZRMOT_DUR           = 0x22, // Zero-Motion Detection Duration
            FIFO_EN             = 0x23, // FIFO Enable
            I2C_MST_CTRL        = 0x24, // I²C Master Control
            I2C_SLV0_ADDR       = 0x25,
            I2C_SLV0_REG        = 0x26,
            I2C_SLV0_CTRL       = 0x27,
            I2C_SLV1_ADDR       = 0x28,
            I2C_SLV1_REG        = 0x29,
            I2C_SLV1_CTRL       = 0x2A,
            I2C_SLV2_ADDR       = 0x2B,
            I2C_SLV2_REG        = 0x2C,
            I2C_SLV2_CTRL       = 0x2D,
            I2C_SLV3_ADDR       = 0x2E,
            I2C_SLV3_REG        = 0x2F,
            I2C_SLV3_CTRL       = 0x30,
            I2C_SLV4_ADDR       = 0x31,
            I2C_SLV4_REG        = 0x32,
            I2C_SLV4_DO         = 0x33,
            I2C_SLV4_CTRL       = 0x34,
            I2C_SLV4_DI         = 0x35,
            I2C_MST_STATUS      = 0x36, // I²C Master Status
            INT_PIN_CFG         = 0x37, // INT Pin / Bypass Enable Configuration
            INT_ENABLE          = 0x38, // Interrupt Enable
            DMP_INT_STATUS      = 0x39,
            INT_STATUS          = 0x3A, // Interrupt Status
            ACCEL_XOUT_H        = 0x3B, // Accelerometer Measurements
            ACCEL_XOUT_L,
            ACCEL_YOUT_H,
            ACCEL_YOUT_L,
            ACCEL_ZOUT_H,
            ACCEL_ZOUT_L,
            TEMP_OUT_H          = 0x41, // Temperature Measurement
            TEMP_OUT_L,
            GYRO_XOUT_H         = 0x43, // Gyroscope Measurements
            GYRO_XOUT_L,
            GYRO_YOUT_H,
            GYRO_YOUT_L,
            GYRO_ZOUT_H,
            GYRO_ZOUT_L,
            EXT_SENS_DATA_00    = 0x49, // External Sensor Data
            EXT_SENS_DATA_01,
            EXT_SENS_DATA_02,
            EXT_SENS_DATA_03,
            EXT_SENS_DATA_04,
            EXT_SENS_DATA_05,
            EXT_SENS_DATA_06,
            EXT_SENS_DATA_07,
            EXT_SENS_DATA_08,
            EXT_SENS_DATA_09,
            EXT_SENS_DATA_10,
            EXT_SENS_DATA_11,
            EXT_SENS_DATA_12,
            EXT_SENS_DATA_13,
            EXT_SENS_DATA_14,
            EXT_SENS_DATA_15,
            EXT_SENS_DATA_16,
            EXT_SENS_DATA_17,
            EXT_SENS_DATA_18,
            EXT_SENS_DATA_19,
            EXT_SENS_DATA_20,
            EXT_SENS_DATA_21,
            EXT_SENS_DATA_22,
            EXT_SENS_DATA_23    = 0x60,
            MOT_DETECT_STATUS   = 0x61,
            I2C_SLV0_DO         = 0x63, // I²C Slave 0 Data Out
            I2C_SLV1_DO,
            I2C_SLV2_DO,
            I2C_SLV3_DO,
            I2C_MST_DELAY_CTRL  = 0x67, // I²C Master Delay Control
            SIGNAL_PATH_RESET   = 0x68, // Signal Path Reset
            MOT_DETECT_CTRL     = 0x69, // Motion Detection Control
            USER_CTRL           = 0x6A, // User Control
            PWR_MGMT_1          = 0x6B, // Power Management 1
            PWR_MGMT_2          = 0x6C, // Power Management 2
            BANK_SEL            = 0x6D,
            MEM_START_ADDR      = 0x6E,
            MEM_R_W             = 0x6F,
            DMP_CFG_1           = 0x70,
            DMP_CFG_2           = 0x71,
            FIFO_COUNT_H        = 0x72, // FIFO Count Registers
            FIFO_COUNT_L,
            FIFO_R_W            = 0x74, // FIFO Read Write
            WHO_AM_I            = 0x75
        };

        explicit MPU6050(uint address = 0x68, QObject *parent = 0);

        bool testConnection();

        // XG_OFFS_TC register
        int8_t getXGyroOffsetTC(); // [6:1]
        void setXGyroOffsetTC(int8_t offset);
        uint8_t getOTPBankValid(); // [0]
        void setOTPBankValid(bool enabled);

        // YG_OFFS_TC register
        // AUX_VDDIO register (InvenSense demo code calls this RA_*G_OFFS_TC)
        uint8_t getAuxVDDIOLevel();
        void setAuxVDDIOLevel(uint8_t level);

        int8_t getYGyroOffsetTC();
        void setYGyroOffsetTC(int8_t offset);

        // ZG_OFFS_TC register
        int8_t getZGyroOffsetTC();
        void setZGyroOffsetTC(int8_t offset);

        // X_FINE_GAIN register
        int8_t getXFineGain();
        void setXFineGain(int8_t gain);

        // Y_FINE_GAIN register
        int8_t getYFineGain();
        void setYFineGain(int8_t gain);

        // Z_FINE_GAIN register
        int8_t getZFineGain();
        void setZFineGain(int8_t gain);

        // XA_OFFS_* registers
        int16_t getXAccelOffset();
        void setXAccelOffset(int16_t offset);

        // YA_OFFS_* registers
        int16_t getYAccelOffset();
        void setYAccelOffset(int16_t offset);

        // ZA_OFFS_* registers
        int16_t getZAccelOffset();
        void setZAccelOffset(int16_t offset);

        // SELF TEST FACTORY TRIM VALUES
        uint8_t getAccelXSelfTestFactoryTrim();
        uint8_t getAccelYSelfTestFactoryTrim();
        uint8_t getAccelZSelfTestFactoryTrim();
        uint8_t getGyroXSelfTestFactoryTrim();
        uint8_t getGyroYSelfTestFactoryTrim();
        uint8_t getGyroZSelfTestFactoryTrim();

        // XG_OFFS_USR* registers
        int16_t getXGyroOffset();
        void setXGyroOffset(int16_t offset);

        // YG_OFFS_USR* registers
        int16_t getYGyroOffset();
        void setYGyroOffset(int16_t offset);

        // ZG_OFFS_USR* registers
        int16_t getZGyroOffset();
        void setZGyroOffset(int16_t offset);

        // SMPLRT_DIV register
        uint8_t getRate();
        void setRate(uint8_t rate);

        // CONFIG register
        uint8_t getExternalFrameSync();
        void setExternalFrameSync(uint8_t sync);
        uint8_t getDLPFMode();
        void setDLPFMode(uint8_t mode);

        // GYRO_CONFIG register
        uint8_t getFullScaleGyroRange();
        void setFullScaleGyroRange(uint8_t range);

        // ACCEL_CONFIG register
        bool getAccelXSelfTest();
        void setAccelXSelfTest(bool enabled);
        bool getAccelYSelfTest();
        void setAccelYSelfTest(bool enabled);
        bool getAccelZSelfTest();
        void setAccelZSelfTest(bool enabled);
        uint8_t getFullScaleAccelRange();
        void setFullScaleAccelRange(uint8_t range);
        uint8_t getDHPFMode();
        void setDHPFMode(uint8_t bandwidth);

        // FF_THR register
        uint8_t getFreefallDetectionThreshold();
        void setFreefallDetectionThreshold(uint8_t threshold);

        // FF_DUR register
        uint8_t getFreefallDetectionDuration();
        void setFreefallDetectionDuration(uint8_t duration);

        // MOT_THR register
        uint8_t getMotionDetectionThreshold();
        void setMotionDetectionThreshold(uint8_t threshold);

        // MOT_DUR register
        uint8_t getMotionDetectionDuration();
        void setMotionDetectionDuration(uint8_t duration);

        // ZRMOT_THR register
        uint8_t getZeroMotionDetectionThreshold();
        void setZeroMotionDetectionThreshold(uint8_t threshold);

        // ZRMOT_DUR register
        uint8_t getZeroMotionDetectionDuration();
        void setZeroMotionDetectionDuration(uint8_t duration);

        // FIFO_EN register
        bool getTempFIFOEnabled();
        void setTempFIFOEnabled(bool enabled);
        bool getXGyroFIFOEnabled();
        void setXGyroFIFOEnabled(bool enabled);
        bool getYGyroFIFOEnabled();
        void setYGyroFIFOEnabled(bool enabled);
        bool getZGyroFIFOEnabled();
        void setZGyroFIFOEnabled(bool enabled);
        bool getAccelFIFOEnabled();
        void setAccelFIFOEnabled(bool enabled);
        bool getSlave2FIFOEnabled();
        void setSlave2FIFOEnabled(bool enabled);
        bool getSlave1FIFOEnabled();
        void setSlave1FIFOEnabled(bool enabled);
        bool getSlave0FIFOEnabled();
        void setSlave0FIFOEnabled(bool enabled);

        // I2C_MST_CTRL register
        bool getMultiMasterEnabled();
        void setMultiMasterEnabled(bool enabled);
        bool getWaitForExternalSensorEnabled();
        void setWaitForExternalSensorEnabled(bool enabled);
        bool getSlave3FIFOEnabled();
        void setSlave3FIFOEnabled(bool enabled);
        bool getSlaveReadWriteTransitionEnabled();
        void setSlaveReadWriteTransitionEnabled(bool enabled);
        uint8_t getMasterClockSpeed();
        void setMasterClockSpeed(uint8_t speed);

        // I2C_SLV* registers (Slave 0-4)
        uint8_t getSlaveAddress(uint8_t num);
        void setSlaveAddress(uint8_t num, uint8_t address);
        uint8_t getSlaveRegister(uint8_t num);
        void setSlaveRegister(uint8_t num, uint8_t reg);
        bool getSlaveEnabled(uint8_t num);
        void setSlaveEnabled(uint8_t num, bool enabled);
        bool getSlaveWordByteSwap(uint8_t num);
        void setSlaveWordByteSwap(uint8_t num, bool enabled);
        bool getSlaveWriteMode(uint8_t num);
        void setSlaveWriteMode(uint8_t num, bool mode);
        bool getSlaveWordGroupOffset(uint8_t num);
        void setSlaveWordGroupOffset(uint8_t num, bool enabled);
        uint8_t getSlaveDataLength(uint8_t num);
        void setSlaveDataLength(uint8_t num, uint8_t length);

        // I2C_SLV* registers (Slave 4)
        void setSlave4OutputByte(uint8_t data);
        bool getSlave4InterruptEnabled();
        void setSlave4InterruptEnabled(bool enabled);
        uint8_t getSlave4MasterDelay();
        void setSlave4MasterDelay(uint8_t delay);
        uint8_t getSlave4InputByte();

        // I2C_MST_STATUS register
        bool getPassthroughStatus();
        bool getSlave4IsDone();
        bool getLostArbitration();
        bool getSlave4Nack();
        bool getSlave3Nack();
        bool getSlave2Nack();
        bool getSlave1Nack();
        bool getSlave0Nack();

        // INT_PIN_CFG register
        bool getInterruptMode();
        void setInterruptMode(bool mode);
        bool getInterruptDrive();
        void setInterruptDrive(bool drive);
        bool getInterruptLatch();
        void setInterruptLatch(bool latch);
        bool getInterruptLatchClear();
        void setInterruptLatchClear(bool clear);
        bool getFSyncInterruptLevel();
        void setFSyncInterruptLevel(bool level);
        bool getFSyncInterruptEnabled();
        void setFSyncInterruptEnabled(bool enabled);
        bool getI2CBypassEnabled();
        void setI2CBypassEnabled(bool enabled);
        bool getClockOutputEnabled();
        void setClockOutputEnabled(bool enabled);

        // INT_ENABLE register
        uint8_t getIntEnabled();
        void setIntEnabled(uint8_t enabled);
        bool getIntFreefallEnabled();
        void setIntFreefallEnabled(bool enabled);
        bool getIntMotionEnabled();
        void setIntMotionEnabled(bool enabled);
        bool getIntZeroMotionEnabled();
        void setIntZeroMotionEnabled(bool enabled);
        bool getIntFIFOBufferOverflowEnabled();
        void setIntFIFOBufferOverflowEnabled(bool enabled);
        bool getIntI2CMasterEnabled();
        void setIntI2CMasterEnabled(bool enabled);
        bool getIntPLLReadyEnabled();
        void setIntPLLReadyEnabled(bool enabled);
        bool getIntDMPEnabled();
        void setIntDMPEnabled(bool enabled);
        bool getIntDataReadyEnabled();
        void setIntDataReadyEnabled(bool enabled);

        // DMP_INT_STATUS register
        uint8_t getDMPIntStatus();
        bool getDMPInt5Status();
        bool getDMPInt4Status();
        bool getDMPInt3Status();
        bool getDMPInt2Status();
        bool getDMPInt1Status();
        bool getDMPInt0Status();

        // INT_STATUS register
        uint8_t getIntStatus();
        bool getIntFreefallStatus();
        bool getIntMotionStatus();
        bool getIntZeroMotionStatus();
        bool getIntFIFOBufferOverflowStatus();
        bool getIntI2CMasterStatus();
        bool getIntPLLReadyStatus();
        bool getIntDMPStatus();
        bool getIntDataReadyStatus();

        // ACCEL_*OUT_* registers
        int16_t getAccelerationX();
        int16_t getAccelerationY();
        int16_t getAccelerationZ();
        void getAcceleration(int16_t* x, int16_t* y, int16_t* z);

        // TEMP_OUT_* registers
        int16_t getTemperature();

        // GYRO_*OUT_* registers
        int16_t getRotationX();
        int16_t getRotationY();
        int16_t getRotationZ();
        void getRotation(int16_t* x, int16_t* y, int16_t* z);

        // EXT_SENS_DATA_* registers
        uint8_t getExternalSensorByte(int position);
        uint16_t getExternalSensorWord(int position);
        uint32_t getExternalSensorDWord(int position);

        // Special Functions
        void getMotion6(int16_t* ax, int16_t* ay, int16_t* az, int16_t* gx, int16_t* gy, int16_t* gz);
        void getMotion9(int16_t* ax, int16_t* ay, int16_t* az, int16_t* gx, int16_t* gy, int16_t* gz, int16_t* mx, int16_t* my, int16_t* mz);

        // MOT_DETECT_STATUS register
        uint8_t getMotionStatus();
        bool getXNegMotionDetected();
        bool getXPosMotionDetected();
        bool getYNegMotionDetected();
        bool getYPosMotionDetected();
        bool getZNegMotionDetected();
        bool getZPosMotionDetected();
        bool getZeroMotionDetected();

        // I2C_SLV*_DO register
        void setSlaveOutputByte(uint8_t num, uint8_t data);

        // I2C_MST_DELAY_CTRL register
        bool getExternalShadowDelayEnabled();
        void setExternalShadowDelayEnabled(bool enabled);
        bool getSlaveDelayEnabled(uint8_t num);
        void setSlaveDelayEnabled(uint8_t num, bool enabled);

        // SIGNAL_PATH_RESET register
        void resetGyroscopePath();
        void resetAccelerometerPath();
        void resetTemperaturePath();

        // MOT_DETECT_CTRL register
        uint8_t getAccelerometerPowerOnDelay();
        void setAccelerometerPowerOnDelay(uint8_t delay);
        uint8_t getFreefallDetectionCounterDecrement();
        void setFreefallDetectionCounterDecrement(uint8_t decrement);
        uint8_t getMotionDetectionCounterDecrement();
        void setMotionDetectionCounterDecrement(uint8_t decrement);

        // USER_CTRL register
        bool getDMPEnabled();
        void setDMPEnabled(bool enabled);
        bool getFIFOEnabled();
        void setFIFOEnabled(bool enabled);
        bool getI2CMasterModeEnabled();
        void setI2CMasterModeEnabled(bool enabled);
        void switchSPIEnabled(bool enabled);
        void resetDMP();
        void resetFIFO();
        void resetI2CMaster();
        void resetSensors();

        // PWR_MGMT_1 register
        void reset();
        bool getSleepEnabled();
        void setSleepEnabled(bool enabled);
        bool getWakeCycleEnabled();
        void setWakeCycleEnabled(bool enabled);
        bool getTempSensorEnabled();
        void setTempSensorEnabled(bool enabled);
        uint8_t getClockSource();
        void setClockSource(uint8_t source);

        // PWR_MGMT_2 register
        uint8_t getWakeFrequency();
        void setWakeFrequency(uint8_t frequency);
        bool getStandbyXAccelEnabled();
        void setStandbyXAccelEnabled(bool enabled);
        bool getStandbyYAccelEnabled();
        void setStandbyYAccelEnabled(bool enabled);
        bool getStandbyZAccelEnabled();
        void setStandbyZAccelEnabled(bool enabled);
        bool getStandbyXGyroEnabled();
        void setStandbyXGyroEnabled(bool enabled);
        bool getStandbyYGyroEnabled();
        void setStandbyYGyroEnabled(bool enabled);
        bool getStandbyZGyroEnabled();
        void setStandbyZGyroEnabled(bool enabled);

        // BANK_SEL register
        void setMemoryBank(uint8_t bank, bool prefetchEnabled = false, bool userBank = false);

        // MEM_START_ADDR
        void setMemoryStartAddress(uint8_t address);

        // MEM_R_W register
        uint8_t readMemoryByte();
        void writeMemoryByte(uint8_t data);
        void readMemoryBlock(uint8_t *data, uint16_t dataSize, uint8_t bank = 0, uint8_t address = 0);
        bool writeMemoryBlock(const uint8_t *data, uint16_t dataSize, uint8_t bank = 0, uint8_t address = 0, bool verify = true, bool useProgMem = false);
        bool writeProgMemoryBlock(const uint8_t *data, uint16_t dataSize, uint8_t bank = 0, uint8_t address = 0, bool verify = true);
        bool writeDMPConfigurationSet(const uint8_t *data, uint16_t dataSize, bool useProgMem = false);
        bool writeProgDMPConfigurationSet(const uint8_t *data, uint16_t dataSize);

        // DMP_CFG_1 register
        uint8_t getDMPConfig1();
        void setDMPConfig1(uint8_t config);

        // DMP_CFG_2 register
        uint8_t getDMPConfig2();
        void setDMPConfig2(uint8_t config);

        // FIFO_COUNT* registers
        uint16_t getFIFOCount();

        // FIFO_R_W register
        uint8_t getFIFOByte();
        void setFIFOByte(uint8_t data);

        // WHO_AM_I register
        uint8_t getDeviceID();
        void setDeviceID(uint8_t id);

    protected:
        void initialize();

    signals:

    public slots:

    private:
        uint8_t         device_id = 0x34;
    };
}

#endif // MPU6050_H
