#ifndef HMC5883L_H
#define HMC5883L_H

#include <QObject>
#include "../i2c.h"




namespace gyro_private {

    class HMC5883L : public QObject, i2c
    {
        Q_OBJECT
    public:
        enum registers {
            CONFIG_A        = 0x00,
            CONFIG_B        = 0x01,
            MODE            = 0x02,
            DATAX_H         = 0x03,
            DATAX_L         = 0x04,
            DATAZ_H         = 0x05,
            DATAZ_L         = 0x06,
            DATAY_H         = 0x07,
            DATAY_L         = 0x08,
            STATUS          = 0x09,
            ID_A            = 0x0A,
            ID_B            = 0x0B,
            ID_C            = 0x0C
        };

        // this device only has one address
        explicit HMC5883L(uint address = 0x1E, QObject *parent = 0);

        // CONFIG_A register
        uint8_t getSampleAveraging();
        void setSampleAveraging(uint8_t averaging);
        uint8_t getDataRate();
        void setDataRate(uint8_t rate);
        uint8_t getMeasurementBias();
        void setMeasurementBias(uint8_t bias);

        // CONFIG_B register
        uint8_t getGain();
        void setGain(uint8_t gain);

        // MODE register
        uint8_t getMode();
        void setMode(uint8_t newMode);

        // DATA* registers
        void getHeading(int16_t *x, int16_t *y, int16_t *z);

        // STATUS register
        bool getLockStatus();
        bool getReadyStatus();

        // ID_* registers
        uint8_t getIDA();
        uint8_t getIDB();
        uint8_t getIDC();

    protected:
        void initialize();

    signals:

    public slots:

    private:
        uint8_t         mode;
        uint8_t         xh, xl, yh, yl, zh, zl;
    };
}

#endif // HMC5883L_H
