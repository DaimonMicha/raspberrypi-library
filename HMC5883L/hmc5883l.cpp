#include "hmc5883l.h"

#include <QDebug>
#include <pigpiod_if2.h>




namespace gyro_private {

    /** InvenSense MPU-6050
     *
     * The MPU6050 combines a 3-axis gyroscope and a 3-axis accelerometer on the same
     * silicon die together with an onboard Digital Motion Processor(DMP) which processes
     * complex 6-axis MotionFusion algorithms
     */
    HMC5883L::HMC5883L(uint address, QObject *parent) :
        QObject(parent),
        i2c(address)
    {
        //pi_device = pigpio_start(NULL, NULL);
        if(pi_device < 0) {
            qDebug() << "pigpio_error:" << pigpio_error(pi_device);
            return;
        }
        //i2c_handle = i2c_open(pi_device, i2c_bus, ic2_address, 0);
        if(i2c_handle < 0) {
            qDebug() << "pigpio_error:" << pigpio_error(i2c_handle);
            return;
        }
        initialize();
    }

    /** Power on and prepare for general usage.
     * This will prepare the magnetometer with default settings, ready for single-
     * use mode (very low power requirements). Default settings include 8-sample
     * averaging, 15 Hz data output rate, normal measurement bias, a,d 1090 gain (in
     * terms of LSB/Gauss). Be sure to adjust any settings you need specifically
     * after initialization, especially the gain settings if you happen to be seeing
     * a lot of -4096 values (see the datasheet for mor information).
     */
    void HMC5883L::initialize() {
        // write CONFIG_A register
        setSampleAveraging(0x03); // 8
        setDataRate(0x04); // 15
        setMeasurementBias(0x00); // Normal

        // write CONFIG_B register
        setGain(1);

        // write MODE register
        setMode(1);
    }

    /** Get number of samples averaged per measurement.
     * @return Current samples averaged per measurement (0-3 for 1/2/4/8 respectively)
     * @see HMC5883L_AVERAGING_8
     * @see HMC5883L_RA_CONFIG_A
     * @see HMC5883L_CRA_AVERAGE_BIT
     * @see HMC5883L_CRA_AVERAGE_LENGTH
     */
    uint8_t HMC5883L::getSampleAveraging() {
        return(readBits(CONFIG_A, 6, 2));
    }

    /** Set number of samples averaged per measurement.
     * @param averaging New samples averaged per measurement setting(0-3 for 1/2/4/8 respectively)
     * @see HMC5883L_RA_CONFIG_A
     * @see HMC5883L_CRA_AVERAGE_BIT
     * @see HMC5883L_CRA_AVERAGE_LENGTH
     */
    void HMC5883L::setSampleAveraging(uint8_t averaging) {
        writeBits(CONFIG_A, 6, 2, averaging);
    }

    /** Get data output rate value.
     * The Table below shows all selectable output rates in continuous measurement
     * mode. All three channels shall be measured within a given output rate. Other
     * output rates with maximum rate of 160 Hz can be achieved by monitoring DRDY
     * interrupt pin in single measurement mode.
     *
     * Value | Typical Data Output Rate (Hz)
     * ------+------------------------------
     * 0     | 0.75
     * 1     | 1.5
     * 2     | 3
     * 3     | 7.5
     * 4     | 15 (Default)
     * 5     | 30
     * 6     | 75
     * 7     | Not used
     *
     * @return Current rate of data output to registers
     * @see HMC5883L_RATE_15
     * @see HMC5883L_RA_CONFIG_A
     * @see HMC5883L_CRA_RATE_BIT (4)
     * @see HMC5883L_CRA_RATE_LENGTH (3)
     */
    uint8_t HMC5883L::getDataRate() {
        return(readBits(CONFIG_A, 4, 3));
    }

    /** Set data output rate value.
     * @param rate Rate of data output to registers
     * @see getDataRate()
     * @see HMC5883L_RATE_15
     * @see HMC5883L_RA_CONFIG_A
     * @see HMC5883L_CRA_RATE_BIT
     * @see HMC5883L_CRA_RATE_LENGTH
     */
    void HMC5883L::setDataRate(uint8_t rate) {
        writeBits(CONFIG_A, 4, 3, rate);
    }

    /** Get measurement bias value.
     * @return Current bias value (0-2 for normal/positive/negative respectively)
     * @see HMC5883L_BIAS_NORMAL
     * @see HMC5883L_RA_CONFIG_A
     * @see HMC5883L_CRA_BIAS_BIT (1)
     * @see HMC5883L_CRA_BIAS_LENGTH (2)
     */
    uint8_t HMC5883L::getMeasurementBias() {
        return(readBits(CONFIG_A, 1, 2));
    }

    /** Set measurement bias value.
     * @param bias New bias value (0-2 for normal/positive/negative respectively)
     * @see HMC5883L_BIAS_NORMAL
     * @see HMC5883L_RA_CONFIG_A
     * @see HMC5883L_CRA_BIAS_BIT
     * @see HMC5883L_CRA_BIAS_LENGTH
     */
    void HMC5883L::setMeasurementBias(uint8_t bias) {
        writeBits(CONFIG_A, 1, 2, bias);
    }

    /** Get magnetic field gain value.
     * The table below shows nominal gain settings. Use the "Gain" column to convert
     * counts to Gauss. Choose a lower gain value (higher GN#) when total field
     * strength causes overflow in one of the data output registers (saturation).
     * The data output range for all settings is 0xF800-0x07FF (-2048 - 2047).
     *
     * Value | Field Range | Gain (LSB/Gauss)
     * ------+-------------+-----------------
     * 0     | +/- 0.88 Ga | 1370
     * 1     | +/- 1.3 Ga  | 1090 (Default)
     * 2     | +/- 1.9 Ga  | 820
     * 3     | +/- 2.5 Ga  | 660
     * 4     | +/- 4.0 Ga  | 440
     * 5     | +/- 4.7 Ga  | 390
     * 6     | +/- 5.6 Ga  | 330
     * 7     | +/- 8.1 Ga  | 230
     *
     * @return Current magnetic field gain value
     * @see HMC5883L_GAIN_1090
     * @see HMC5883L_RA_CONFIG_B
     * @see HMC5883L_CRB_GAIN_BIT
     * @see HMC5883L_CRB_GAIN_LENGTH
     */
    uint8_t HMC5883L::getGain() {
        return(readBits(CONFIG_B, 7, 3));
    }

    /** Set magnetic field gain value.
     * @param gain New magnetic field gain value
     * @see getGain()
     * @see HMC5883L_RA_CONFIG_B
     * @see HMC5883L_CRB_GAIN_BIT (7)
     * @see HMC5883L_CRB_GAIN_LENGTH (3)
     */
    void HMC5883L::setGain(uint8_t gain) {
        // use this method to guarantee that bits 4-0 are set to zero, which is a
        // requirement specified in the datasheet; it's actually more efficient than
        // using the I2Cdev.writeBits method
        writeByte(CONFIG_B, gain << (7 - 3 + 1));
    }

    /** Get measurement mode.
     * In continuous-measurement mode, the device continuously performs measurements
     * and places the result in the data register. RDY goes high when new data is
     * placed in all three registers. After a power-on or a write to the mode or
     * configuration register, the first measurement set is available from all three
     * data output registers after a period of 2/fDO and subsequent measurements are
     * available at a frequency of fDO, where fDO is the frequency of data output.
     *
     * When single-measurement mode (default) is selected, device performs a single
     * measurement, sets RDY high and returned to idle mode. Mode register returns
     * to idle mode bit values. The measurement remains in the data output register
     * and RDY remains high until the data output register is read or another
     * measurement is performed.
     *
     * @return Current measurement mode
     * @see HMC5883L_MODE_CONTINUOUS
     * @see HMC5883L_MODE_SINGLE
     * @see HMC5883L_MODE_IDLE
     * @see HMC5883L_RA_MODE
     * @see HMC5883L_MODEREG_BIT
     * @see HMC5883L_MODEREG_LENGTH
     */
    uint8_t HMC5883L::getMode() {
        return(readBits(MODE, 1, 2));
    }

    /** Set measurement mode.
     * @param newMode New measurement mode
     * @see getMode()
     * @see HMC5883L_MODE_CONTINUOUS (0x00)
     * @see HMC5883L_MODE_SINGLE (0x01)
     * @see HMC5883L_MODE_IDLE (0x02)
     * @see MODE
     * @see HMC5883L_MODEREG_BIT (1)
     * @see HMC5883L_MODEREG_LENGTH (2)
     */
    void HMC5883L::setMode(uint8_t newMode) {
        // use this method to guarantee that bits 7-2 are set to zero, which is a
        // requirement specified in the datasheet; it's actually more efficient than
        // using the I2Cdev.writeBits method
        writeByte(MODE, newMode << (1 - 2 + 1));
        mode = newMode; // track to tell if we have to clear bit 7 after a read
    }

    /** Get 3-axis heading measurements.
     * In the event the ADC reading overflows or underflows for the given channel,
     * or if there is a math overflow during the bias measurement, this data
     * register will contain the value -4096. This register value will clear when
     * after the next valid measurement is made. Note that this method automatically
     * clears the appropriate bit in the MODE register if Single mode is active.
     * @param x 16-bit signed integer container for X-axis heading
     * @param y 16-bit signed integer container for Y-axis heading
     * @param z 16-bit signed integer container for Z-axis heading
     * @see DATAX_H
     */
    void HMC5883L::getHeading(int16_t *x, int16_t *y, int16_t *z) {
        xh = readByte(DATAX_H);
        xl = readByte(DATAX_L);
        zh = readByte(DATAZ_H);
        zl = readByte(DATAZ_L);
        yh = readByte(DATAY_H);
        yl = readByte(DATAY_L);
        if(mode == 0x01) setMode(0x01);
        *x = (((int16_t)xh) << 8) | xl;
        *y = (((int16_t)yh) << 8) | yl;
        *z = (((int16_t)zh) << 8) | zl;
    }

    /** Get data output register lock status.
     * This bit is set when this some but not all for of the six data output
     * registers have been read. When this bit is set, the six data output registers
     * are locked and any new data will not be placed in these register until one of
     * three conditions are met: one, all six bytes have been read or the mode
     * changed, two, the mode is changed, or three, the measurement configuration is
     * changed.
     * @return Data output register lock status
     * @see STATUS
     */
    bool HMC5883L::getLockStatus() {
        return(readBit(STATUS, 1));
    }

    /** Get data ready status.
     * This bit is set when data is written to all six data registers, and cleared
     * when the device initiates a write to the data output registers and after one
     * or more of the data output registers are written to. When RDY bit is clear it
     * shall remain cleared for 250 us. DRDY pin can be used as an alternative to
     * the status register for monitoring the device for measurement data.
     * @return Data ready status
     * @see STATUS
     */
    bool HMC5883L::getReadyStatus() {
        return(readBit(STATUS, 0));
    }

    /** Get identification byte A
     * @return ID_A byte (should be 01001000, ASCII value 'H')
     */
    uint8_t HMC5883L::getIDA() {
        return(readByte(ID_A));
    }

    /** Get identification byte B
     * @return ID_A byte (should be 00110100, ASCII value '4')
     */
    uint8_t HMC5883L::getIDB() {
        return(readByte(ID_B));
    }

    /** Get identification byte C
     * @return ID_A byte (should be 00110011, ASCII value '3')
     */
    uint8_t HMC5883L::getIDC() {
        return(readByte(ID_C));
    }

}
