#include "i2c.h"
#include <pigpiod_if2.h>




i2c::i2c(uint & address) :
    ic2_address(address)
{
    pi_device = pigpio_start(NULL, NULL);
    i2c_handle = i2c_open(pi_device, i2c_bus, ic2_address, 0);
}

i2c::~i2c()
{
    i2c_close(pi_device, i2c_handle);
    pigpio_stop(pi_device);
}

/** Read a single bit from an 8-bit device register.
 * @param reg Register to read from
 * @param bit Bit position to read (0-7)
 * @return single bit value
 */
bool i2c::readBit(uint8_t reg, uint8_t bit)
{
    uint8_t byte = readByte(reg);
    return((byte & (1 << bit)) == 0);
}

/** Read multiple bits from an 8-bit device register.
 * @param reg Register regAddr to read from
 * @param bit First bit position to read (0-7)
 * @param length Number of bits to read (not more than 8)
 * @return right-aligned value (i.e. '101' read from any bitStart position will equal 0x05)
 */
uint8_t i2c::readBits(uint8_t reg, uint8_t bit, uint8_t length)
{
    uint8_t byte = readByte(reg);
    uint8_t mask = ((1 << length) - 1) << (bit - length + 1);
    byte &= mask;
    byte >>= (bit - length + 1);
    return(byte);
}

/** Read single byte from an 8-bit device register.
 * @param reg Register regAddr to read from
 * @return byte value read from device
 */
uint8_t i2c::readByte(uint8_t reg)
{
    return(i2c_read_byte_data(pi_device, i2c_handle, reg));
}

/** write a single bit in an 8-bit device register.
 * @param reg Register regAddr to write to
 * @param bit Bit position to write (0-7)
 * @param data New bit value to write
 */
void i2c::writeBit(uint8_t reg, uint8_t bit, bool data)
{
    uint8_t byte = readByte(reg);
    byte = data ? (byte & ~(1 << bit)) : (byte | (1 << bit));
    writeByte(reg, byte);
}

/** Write multiple bits in an 8-bit device register.
 * @param reg Register reg to write to
 * @param bit First bit position to write (0-7)
 * @param length Number of bits to write (not more than 8)
 * @param data Right-aligned value to write
 */
void i2c::writeBits(uint8_t reg, uint8_t bit, uint8_t length, uint8_t data)
{
    uint8_t byte = readByte(reg);
    uint8_t mask = ((1 << length) - 1) << (bit - length + 1);
    data <<= (bit - length + 1); // shift data into correct position
    data &= mask; // zero all non-important bits in data
    byte &= ~(mask); // zero all important bits in existing byte
    byte |= data; // combine data with existing byte
    writeByte(reg, byte);
}

/** Write single byte to an 8-bit device register.
 * @param reg Register address to write to
 * @param data New byte value to write
 */
void i2c::writeByte(uint8_t reg, uint8_t data)
{
    i2c_write_byte_data(pi_device, i2c_handle, reg, data);
}

