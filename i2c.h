#ifndef I2C_H
#define I2C_H

#include <cstdlib>
#include <cstdint>


class i2c
{
public:
    explicit i2c(uint & address);
    ~i2c();

protected:
    bool readBit(uint8_t reg, uint8_t bit);
    uint8_t readBits(uint8_t reg, uint8_t bit, uint8_t length);
    uint8_t readByte(uint8_t reg);
    void writeBit(uint8_t reg, uint8_t bit, bool data);
    void writeBits(uint8_t reg, uint8_t bit, uint8_t length, uint8_t data);
    void writeByte(uint8_t reg, uint8_t data);

protected:
    uint            ic2_address;
    int             pi_device = -1;
    int             i2c_bus = 1;
    int             i2c_handle = -1;
};

#endif // I2C_H
